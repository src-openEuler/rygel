%global apiver  2.8

Name:          rygel
Version:       0.42.4
Release:       2
Summary:       A media solution can easily share audio, video and pictures, and media player controler
License:       LGPLv2+
URL:           https://wiki.gnome.org/Projects/Rygel
Source0:       https://download.gnome.org/sources/%{name}/0.40/%{name}-%{version}.tar.xz

Patch0001:     0001-fix-the-title-bar-close-button-not-responding.patch

BuildRequires: dbus-glib-devel desktop-file-utils docbook-style-xsl gettext gobject-introspection-devel
BuildRequires: gst-editing-services-devel gstreamer1-devel gstreamer1-plugins-base-devel gtk-doc gtk3-devel
BuildRequires: gupnp-devel gupnp-av-devel gupnp-dlna-devel libgee-devel libmediaart-devel libsoup-devel
BuildRequires: libunistring-devel libuuid-devel meson sqlite-devel systemd-devel tracker3-devel vala
BuildRequires: libmediaart-help libxslt

Provides:   %{name}-tracker = %{version}-%{release}
Obsoletes:  %{name}-tracker < %{version}-%{release}

%description
Rygel is a home media solution that allows you to easily share audio, video and
pictures, and control of media player on your home network. In technical terms
it is both a UPnP AV MediaServer and MediaRenderer implemented through a plug-in
mechanism. Interoperability with other devices in the market is achieved by
conformance to very strict requirements of DLNA and on the fly conversion of
media to format that client devices are capable of handling.

%package devel
Summary: Development package for %{name}
Requires: %{name}%{?_isa} = %{version}-%{release}

%description devel
Files for development with %{name}.

%package help
Summary: Help package for %{name}

%description help
Files for help with %{name}.

%prep
%autosetup -p1

%build
%meson \
  -Dapi-docs=true \
  -Dexamples=false
%meson_build

%install
%meson_install

%find_lang %{name}

%check
desktop-file-validate %{buildroot}/%{_datadir}/applications/rygel.desktop
desktop-file-validate %{buildroot}/%{_datadir}/applications/rygel-preferences.desktop

%files -f %{name}.lang
%config(noreplace) %{_sysconfdir}/rygel.conf
%{_bindir}/rygel
%{_bindir}/rygel-preferences
%{_libdir}/librygel-core-%{apiver}.so.0*
%{_libdir}/librygel-db-%{apiver}.so.0*
%{_libdir}/librygel-renderer-%{apiver}.so.0*
%{_libdir}/librygel-renderer-gst-%{apiver}.so.0*
%{_libdir}/librygel-ruih-%{apiver}.so.0*
%{_libdir}/librygel-server-%{apiver}.so.0*
%dir %{_libdir}/girepository-1.0
%{_libdir}/girepository-1.0/RygelCore-%{apiver}.typelib
%{_libdir}/girepository-1.0/RygelRenderer-%{apiver}.typelib
%{_libdir}/girepository-1.0/RygelRendererGst-%{apiver}.typelib
%{_libdir}/girepository-1.0/RygelServer-%{apiver}.typelib
%{_libdir}/rygel-%{apiver}/engines/librygel-media-engine-gst.so
%{_libdir}/rygel-%{apiver}/engines/librygel-media-engine-simple.so
%{_libdir}/rygel-%{apiver}/engines/media-engine-gst.plugin
%{_libdir}/rygel-%{apiver}/engines/media-engine-simple.plugin
%{_libdir}/rygel-%{apiver}/plugins/librygel-external.so
%{_libdir}/rygel-%{apiver}/plugins/external.plugin
%{_libdir}/rygel-%{apiver}/plugins/librygel-gst-launch.so
%{_libdir}/rygel-%{apiver}/plugins/gst-launch.plugin
%{_libdir}/rygel-%{apiver}/plugins/librygel-media-export.so
%{_libdir}/rygel-%{apiver}/plugins/media-export.plugin
%{_libdir}/rygel-%{apiver}/plugins/librygel-mpris.so
%{_libdir}/rygel-%{apiver}/plugins/mpris.plugin
%{_libdir}/rygel-%{apiver}/plugins/librygel-ruih.so
%{_libdir}/rygel-%{apiver}/plugins/ruih.plugin
%{_libdir}/rygel-%{apiver}/plugins/librygel-playbin.so
%{_libdir}/rygel-%{apiver}/plugins/playbin.plugin
%{_libdir}/rygel-%{apiver}/plugins/librygel-lms.so
%{_libdir}/rygel-%{apiver}/plugins/lms.plugin
%{_libexecdir}/rygel/
%{_datadir}/rygel/
%{_datadir}/applications/rygel*
%{_datadir}/dbus-1/services/org.gnome.Rygel1.service
%{_datadir}/icons/hicolor/*/apps/rygel*
%{_mandir}/man1/rygel.1*
%{_mandir}/man5/rygel.conf.5*
%{_userunitdir}/rygel.service

%files devel
%{_includedir}/rygel-%{apiver}
%{_libdir}/librygel-*.so
%{_libdir}/rygel-%{apiver}/plugins/librygel-tracker3.so
%{_libdir}/rygel-%{apiver}/plugins/tracker3.plugin
%{_libdir}/pkgconfig/rygel*.pc
%{_datadir}/gir-1.0/*
%{_datadir}/vala/vapi/*

%files help
%dir %{_datadir}/gtk-doc
%dir %{_datadir}/gtk-doc/html
%doc %{_datadir}/gtk-doc/html/librygel*
%{_mandir}/man1/rygel.1*
%{_mandir}/man5/rygel.conf.5*

%changelog
* Thu Dec 12 2024 yangfei <yangfei@uniontech.com> - 0.42.4-2
- fix: fix the title bar close button not responding

* Wed Aug 23 2023 konglidong <konglidong@uniontech.com> - 0.42.4-1
- update to 0.42.4

* Mon Mar 28 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 0.40.3-1
- Update to 0.40.3

* Fri Sep 24 2021 Wenlong Ding <wenlong.ding@turbolinux.com.cn> - 0.40.1-2
- Build tracker 3.0 plugin, disable tracker 2.0

* Wed Jun 23 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 0.40.1-1
- Package init with version 0.40.1
